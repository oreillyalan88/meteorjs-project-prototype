Template.profile.rendered = function() {
    $("#profile-link").addClass('selected');
    $("#feed-link").removeClass('selected');
    $("#rankings-link").removeClass('selected');
    $("#search-link").removeClass('selected');
    $("#login-link").removeClass('selected');
}

Template.profile.helpers({
    email: function(){
        if(!Meteor.user()){
            Bert.alert("You are not logged in, permission denied", "danger", "growl-top-right");
        }else{
            return Meteor.user().emails[0].address;
        }
        
    },
    
    username: function(){
             if(!Meteor.user()){
            Bert.alert("You are not logged in, permission denied", "danger", "growl-top-right");
        }else{
            return Meteor.user().username;
        }
        
    },
    
    
    userInteraction: function(){
        var username =Meteor.user().username;
        var userId = Meteor.userId();
        var userInteraction = Interaction.find({userId: userId}, {sort: {createdAt: -1}});
        return userInteraction;
    },
    
    userUpVoteScore: function(){
        return Meteor.user().profile.upVoteScore;
    },
    
    userDownVoteScore: function(){
        return Meteor.user().profile.downVoteScore;
    },
    
    
});

Template.profile.events({
    "click #delete-interaction": function(){
        Meteor.call("removeInteraction", this._id);
        Bert.alert("Your Interaction was Deleted", "success", "growl-top-right")
    }
})