Template.interaction.rendered = function() {
    $("#feed-link").addClass('selected');
    $("#profile-link").removeClass('selected');
    $("#rankings-link").removeClass('selected');
    $("#search-link").removeClass('selected');
    $("#login-link").removeClass('selected');
}


Template.interaction.helpers({
    interaction: function(){
        var interaction = Interaction.find({}, {sort: {createdAt: -1}});
        return interaction;
    }
});

Template.interaction.events({
    "click #up":function(){
        var thisUser = Meteor.userId();
        var thisInteraction = Interaction.findOne({_id: this._id})._id;
        var interactionAuthor = Interaction.findOne({_id: this._id}).userId;
        var Name = Meteor.user().username;
        var thisInteractionVotes = Interaction.findOne({_id: this._id}, {votes: {$in: Name}}).votes;
        
        console.log(thisInteraction);
        
        
        if (thisInteractionVotes.indexOf(Name) > -1) {
            Bert.alert("You cannot vote twice", "danger", "growl-top-right");
            // In the array", meaning user has voted
            
        } 
        else {
            //Not in the array, do stuff
            Meteor.call("countVote", thisInteraction, Name)
            Meteor.call("userPointUpVote", interactionAuthor)
            Meteor.call("upVoteVote",thisUser , thisInteraction)
            Bert.alert("Your vote was Placed", "success", "growl-top-right");

            
        }if(Name == thisInteractionVotes) {
            Bert.alert("You can not Vote for Yourself", "danger", "growl-top-right");
        }
        
    },
    
        
	"click #down": function() {
		var thisUser = Meteor.userId();
		var thisInteraction = Interaction.findOne({_id: this._id})._id;
		var interactionAuthor = Interaction.findOne({_id: this._id}).userId;
		var Name = Meteor.user().username;
		var thisInteractionVotes = Interaction.findOne({_id: this._id}, {voted: {$in: Name}}).votes;

		if (thisInteractionVotes.indexOf(Name) > -1) {
			Bert.alert("You cannot vote twice", "danger", "growl-top-right");
			// In the array!, meaning user has voted
		} else {
			// Not in the Array, Do stuff.
			Meteor.call("countVote", thisInteraction, Name);
			Meteor.call("userPointDownVote", interactionAuthor)
            Meteor.call("downVoteVote",thisUser , thisInteraction)
			Bert.alert("Your Vote Was Placed", "success", "growl-top-right");

		}

		if (Name == thisInteractionVotes) {
			Bert.alert("You cannot Down Vote Yourself", "danger", "growl-top-right");
		}	
	},
    
});