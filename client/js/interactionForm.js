Template.interactionForm.rendered = function(){}



Template.interactionForm.events({
    "submit .interaction-post": function(){
        var interactionName = event.target.interactionName.value;
        var interactionPost = event.target.interactionPost.value;
        
        if(isNotEmpty(interactionName) &&
            isNotEmpty(interactionPost)) {
           
           Meteor.call('addInteraction', interactionName, interactionPost);
           
           event.target.interactionName.value ="";
           event.target.interactionPost.value ="";          
           
           Bert.alert("Your Comment was Posted!", "success", "growl-top-right");
           
        }   else{
            
           Bert.alert("Something went wrong", "danger", "growl-top-right");
           
        }
        
        return false; //prevent submit
    }
});


// Validation Rules

var isNotEmpty = function(value){
	if (value && value !== ''){
		return true;
	}
	Bert.alert("Please fill in all fields", "danger", "growl-top-right");
	return false;
};