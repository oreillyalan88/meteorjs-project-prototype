Template.rankings.rendered = function() {
    $("#rankings-link").addClass('selected');
    $("#profile-link").removeClass('selected');
    $("#feed-link").removeClass('selected');
    $("#search-link").removeClass('selected');
    $("#login-link").removeClass('selected');
}


Template.rankings.helpers({
    mostUpvotes: function(){
        var mostUpvotes = Meteor.users.findOne({}, {sort: {'profile.upVote': -1}}); // there can only be one!
        return mostUpvotes;
    },
    
    mostDownvotes: function(){
        var downUpvotes = Meteor.users.findOne({}, {sort: {'profile.downVote': -1}}); // there can only be one!
        return downUpvotes;
    },
    
});