if (Meteor.isServer){
    Meteor.methods({
        //Meteor for adding interactions
        addInteraction: function(interactionName, interactionPost){
            if(!Meteor.userId()){
                throw new Meteor.Error('not authorizes');
                return false;
            }else {
                var username = Meteor.user().username;
                var year = new Date().getFullYear();
                var month = new Date().getMonth() + 1;
                var day = new Date().getDate();
                var date = (month + "/" + day + "/" + year).toString();
                
                Interaction.insert({
                    interactionName: interactionName,
                    interactionPost: interactionPost,
                    auther: username,
                    date: date,
                    createdAt: new Date(),
                    upVoteScore: 0,
                    downVoteScore: 0,
                    votes: [username],
                    userId: Meteor.userId(),
                });
                
            }
            
        },
        
        removeInteraction: function(interactionId) {
            if(!Meteor.userId()) {
                throw new Meteor.Error('not authorized');
                this.stop();
                return false;
            } else {
                Interaction.remove(interactionId);
            }
        },
        
        countVote: function(thisInteraction, Name) {
			if(!Meteor.userId()) {
				throw new Meteor.Error('not authorized');
				this.stop();
				return false;
			} else {
				Interaction.update(thisInteraction, { $addToSet: { votes: Name}});
			}
		},
		
		userPointUpVote: function(interactionAuthor) {
			if(!Meteor.userId()) {
				throw new Meteor.Error('not authorized');
				this.stop();
				return false;
			} else {
				Meteor.users.update(interactionAuthor, { $inc: {'profile.upVoteScore': +1}});
			}
		},
		
		userPointDownVote: function(interactionAuthor) {
			if(!Meteor.userId()) {
				throw new Meteor.Error('not authorized');
				this.stop();
				return false;
			} else {
				Meteor.users.update(interactionAuthor, { $inc: {'profile.downVoteScore': +1}});
			}
		},
		
		upVoteVote: function(thisUser, thisInteraction) {
			if (!thisUser) {
				throw new Meteor.Error('not authorized');
				return false;
			} else {
				Interaction.update(thisInteraction, {$inc: {upVoteScore: +1}});
			}
		},
        
        downVoteVote: function(thisUser, thisInteraction) {
			if (!thisUser) {
				throw new Meteor.Error('not authorized');
				return false;
			} else {
				Interaction.update(thisInteraction, {$inc: {downVoteScore: +1}});
			}
		},
        
    });
}