if (Meteor.isServer) {
    Meteor.publish('Interaction', function(){
        if(!this.userId){
            return false;
            throw new Meteor.Error('not authorized');
        } else{
            return Interaction.find();
        }
    });
    

    Meteor.publish('Users', function(){
        if(!this.userId){
            return false;
            throw new Meteor.Error('not authorized');
        } else{
            return Meteor.users.find();
        }
    });
}